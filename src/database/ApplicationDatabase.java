package database;



import java.util.ArrayList;




import com.example.bundstest.beans.Season;
import com.example.bundstest.beans.Team;
import com.example.bundstest.beans.TeamFixture;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author Mahmoud Galal <br>
 * Database implementation 
 */
public class ApplicationDatabase extends SQLiteOpenHelper {
	
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "BundesLiga.db";
    public static final String DATABASE_FILE_NAME = "BundesLiga.db";
    private static String DBTAG="App_DataBase";
    
    private static final  String CREATE_PREFIX="CREATE TABLE IF NOT EXISTS ";
    
    private Context mContext;
	
	
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                             Season Table
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final String CREATE_SEASONS_TABLE = CREATE_PREFIX+Season.TABLE_NAME
    		+"("
    		+	Season.ID_KEY+"  INTEGER PRIMARY KEY ASC,"
    		+   Season.URL_ID + " INTEGER"    		
    		+")";
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                             TEAMS Table
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final String CREATE_TEAMS_TABLE = CREATE_PREFIX+Team.TABLE_NAME
    		+"("
    		+	Team.ID_KEY+"  INTEGER PRIMARY KEY ASC,"
    		+   Team.NAME_KEY + " TEXT,"
    		+   Team.PHOTO_KEY + " TEXT,"
    		+	Team.FIXTURES_URL_KEY + " TEXT"    		
    		+")";
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                             TEAM_FIXTURES Table
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final String CREATE_TEAM_FIXTURES_TABLE = CREATE_PREFIX+TeamFixture.TABLE_NAME
	+"("
	+	TeamFixture.ID_KEY+"  INTEGER PRIMARY KEY ASC,"
	+   TeamFixture.TEAM_ID_KEY + " INTEGER," 
	+   TeamFixture.SEASON_ID_KEY + " INTEGER,"
	+   TeamFixture.HOME_GOALS_KEY + " INTEGER,"
	+   TeamFixture.AWAY_GOALS_KEY + " INTEGER,"
	+   TeamFixture.HOME_TEAM_NAME_KEY + " TEXT,"
	+   TeamFixture.AWAY_TEAM_NAME_KEY + " TEXT,"	
	+   TeamFixture.HOME_TEAM_PHOTO_KEY + " TEXT,"
	+   TeamFixture.AWAY_TEAM_PHOTO_KEY + " TEXT,"	
	+ "FOREIGN KEY("+ TeamFixture.TEAM_ID_KEY +") REFERENCES "+ Team.TABLE_NAME
    + "("+Team.ID_KEY+") ON DELETE CASCADE ,"
    + "FOREIGN KEY("+ TeamFixture.SEASON_ID_KEY +") REFERENCES "+ Season.TABLE_NAME
    + "("+Season.ID_KEY+") ON DELETE CASCADE "
	+")";
	
	
	private static final String[] SQL_DELETE_ALL =
		{
		   "DELETE FROM "+Season.TABLE_NAME,
		   "DELETE FROM "+Team.TABLE_NAME,
		   "DELETE FROM "+TeamFixture.TABLE_NAME
		};
	
	
	

	public ApplicationDatabase(Context context) {
		super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
		mContext = context;
	}
	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		db.execSQL("PRAGMA foreign_keys = ON;"); 
	}

	

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		Log.i(DBTAG, "Creating tables");
		db.execSQL(CREATE_SEASONS_TABLE);
		db.execSQL(CREATE_TEAMS_TABLE);
		db.execSQL(CREATE_TEAM_FIXTURES_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//onCreate(db);
	}
	
	public String getSqliteVersion()
	{
		String query="SELECT sqlite_version() AS \'SQLite Version\'";
		SQLiteDatabase db = getReadableDatabase();
		String version="";
		Cursor c =  db.rawQuery(query, null);
		if(c != null)
		{
			while(c.moveToNext())
			{
				version = c.getString(0);
			}
			c.close();
		}
		db.close();
		return version;
	}
	
	public void deleteAllData()
	{
		SQLiteDatabase db = getWritableDatabase();
		for(String st:SQL_DELETE_ALL)
			db.execSQL(st);
		db.close();
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//								Season Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public long insertSeason(Season season)
	{
		SQLiteDatabase db = getWritableDatabase();
		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(Season.URL_ID, season.getUrlID());			

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(
				Season.TABLE_NAME,
		         null,
		         values);
		Log.i(DBTAG, "Season item inserted at:"+newRowId);
		season.setId(newRowId);
		db.close();
		return newRowId;
	}
	
	public boolean isSeasonExist(int urlID)
	{
		String selection = Season.URL_ID + " = ?";
		String[] selectionArgs = { String.valueOf(urlID) };
		String orderBy = Season.ID_KEY;	
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor c = db.query(Season.TABLE_NAME, null, selection, selectionArgs,
				null, null, orderBy);
		if(c != null )
		{
			while(c.moveToNext())
			{
				c.close();
				db.close();
				return true;
			}
			c.close();
			db.close();
		}
		return false;
	}
	
	public Season getSeasonByUrlID(int urlID)
	{
		String selection = Season.URL_ID + " = ?";
		String[] selectionArgs = { String.valueOf(urlID) };
		String orderBy = Season.ID_KEY;	
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor c = db.query(Season.TABLE_NAME, null, selection, selectionArgs,
				null, null, orderBy);
		if(c != null )
		{
			while(c.moveToNext())
			{
				Season season = new Season();
				long id = c.getLong(c.getColumnIndex(Season.ID_KEY));
				season.setId(id);
				season.setUrlID(urlID);
				c.close();
				db.close();
				return season;
			}
			c.close();
			db.close();
		}
		
		return null;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//								Team Operations
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public long insertTeam(Team team)
	{
		SQLiteDatabase db = getWritableDatabase();
		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(Team.FIXTURES_URL_KEY, team.getFixturesUrl());	
		values.put(Team.NAME_KEY, team.getTeamName());	
		values.put(Team.PHOTO_KEY, team.getTeamPhoto());	
		

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(
				Team.TABLE_NAME,
		         null,
		         values);
		Log.i(DBTAG, "Team item inserted at:"+newRowId);
		team.setId(newRowId);
		db.close();
		return newRowId;
	}
	
	
	public ArrayList<Team> getAllTeams()
	{
		
		String orderBy = Team.ID_KEY;
		
		ArrayList<Team> list =  new ArrayList<Team>();
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor c = db.query(Team.TABLE_NAME, null, null, null,
				null, null, orderBy);
		if(c != null )
		{
			while(c.moveToNext())
			{
				Team team = new Team();
				
				String teamName = c.getString(c.getColumnIndex(Team.NAME_KEY));	
				String teamFixturesUrl = c.getString(c.getColumnIndex(Team.FIXTURES_URL_KEY));
				String teamPhoto = c.getString(c.getColumnIndex(Team.PHOTO_KEY));				
				long id = c.getLong(c.getColumnIndex(Team.ID_KEY));
				
				Log.i(DBTAG, "Team ID:"+id);
				
				team.setId(id);
				team.setTeamName(teamName);
				team.setTeamPhoto(teamPhoto);
				team.setFixturesUrl(teamFixturesUrl);				
				list.add(team);
				//allAppointments.add(appt);				
			}
			c.close();
		}
		
		db.close();
		
		return list;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//								TeamFixtures Operations
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public long insertTeamFixture(TeamFixture teamFix)
	{
		SQLiteDatabase db = getWritableDatabase();
		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(TeamFixture.HOME_TEAM_NAME_KEY, teamFix.getHomeTeamName());	
		values.put(TeamFixture.AWAY_TEAM_NAME_KEY, teamFix.getAwayTeamName());	
		values.put(TeamFixture.AWAY_TEAM_PHOTO_KEY, teamFix.getAwayTeamPhoto());	
		values.put(TeamFixture.HOME_TEAM_PHOTO_KEY, teamFix.getHomeTeamPhoto());
		values.put(TeamFixture.HOME_GOALS_KEY, teamFix.getGoalsHomeTeam());
		values.put(TeamFixture.AWAY_GOALS_KEY, teamFix.getGoalsAwayTeam());
		values.put(TeamFixture.TEAM_ID_KEY, teamFix.getTeamId());
		values.put(TeamFixture.SEASON_ID_KEY, teamFix.getSeasonId());
		

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(
				TeamFixture.TABLE_NAME,
		         null,
		         values);
		Log.i(DBTAG, "TeamFixture item inserted at:"+newRowId);
		teamFix.setId(newRowId);
		db.close();
		return newRowId;
	}
	
	public ArrayList<TeamFixture> getTeamFixtures(long teamID)
	{
		String selection = TeamFixture.TEAM_ID_KEY + " = ?";
		String[] selectionArgs = { String.valueOf(teamID) };
		String orderBy = TeamFixture.ID_KEY;
		
		ArrayList<TeamFixture> list =  new ArrayList<TeamFixture>();
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor c = db.query(TeamFixture.TABLE_NAME, null, selection, selectionArgs,
				null, null, orderBy);
		if(c != null )
		{
			while(c.moveToNext())
			{
				TeamFixture teamFix = new TeamFixture();
				
				String homeTeamName = c.getString(c.getColumnIndex(TeamFixture.HOME_TEAM_NAME_KEY));	
				String awayTeamName = c.getString(c.getColumnIndex(TeamFixture.AWAY_TEAM_NAME_KEY));
				String homeTeamPhoto = c.getString(c.getColumnIndex(TeamFixture.HOME_TEAM_PHOTO_KEY));
				String awayTeamPhoto = c.getString(c.getColumnIndex(TeamFixture.AWAY_TEAM_PHOTO_KEY));
				
				int awayGoals = c.getInt(c.getColumnIndex(TeamFixture.AWAY_GOALS_KEY));				
				int homeGoals = c.getInt(c.getColumnIndex(TeamFixture.HOME_GOALS_KEY));	
				//long teamId = c.getLong(c.getColumnIndex(TeamFixture.TEAM_ID_KEY));
				long seasonId = c.getLong(c.getColumnIndex(TeamFixture.SEASON_ID_KEY));
				long id = c.getLong(c.getColumnIndex(TeamFixture.ID_KEY));
				
				Log.i(DBTAG, "TeamFixture ID:"+id);
				
				teamFix.setId(id);
				teamFix.setAwayTeamName(awayTeamName);
				teamFix.setAwayTeamPhoto(awayTeamPhoto);
				teamFix.setGoalsAwayTeam(awayGoals);
				teamFix.setHomeTeamName(homeTeamName);
				teamFix.setHomeTeamPhoto(homeTeamPhoto);
				teamFix.setGoalsHomeTeam(homeGoals);
				teamFix.setTeamId(teamID);
				teamFix.setSeasonId(seasonId);
				
				list.add(teamFix);
				//allAppointments.add(appt);				
			}
			c.close();
		}
		
		db.close();
		
		return list;
		
	}
	
	
	
	
	
	
	
	
	
	

}
