package com.example.bundstest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utils.AppUtilities;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGImageView;
import com.example.bundstest.beans.Season;
import com.example.bundstest.beans.Team;









import database.ApplicationDatabase;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

/**
 * 
 * @author Mahmoud Galal
 *
 */

public class MainActivity extends Activity {

	private ArrayList<Team> allTeams;
	private TeamsAdapter teamsAdapter;
	private PlaceholderFragment fragment;
	private static Season season;
	public static final String SEASON_ID_KEY= MainActivity.class.getName()+".SEASON_ID_KEY";
	public static final String TEAM_DETAILS_KEY= MainActivity.class.getName()+".Details";
	public static final String  CURRENT_SEASON_URL = "http://api.football-data.org/alpha/soccerseasons/351";
	public static final int  CURRENT_SEASON_URL_ID = 351;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_main);
		fragment = new PlaceholderFragment();
		allTeams = new ArrayList<Team>() ;
		teamsAdapter = new TeamsAdapter(this, allTeams);
		setProgressBarVisibility(true);
		setProgressBarIndeterminate(true);
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();
		}
		fragment.setTeamListAdapter(teamsAdapter);
		
		season = new Season();
		season.setUrlID(CURRENT_SEASON_URL_ID);
		ApplicationDatabase db = new ApplicationDatabase(this);
		
		if(!isInDatabase())
		{	
			if(AppUtilities.isInternetConnectionExist(this))
			{
			   db.insertSeason(season);
			   startRequest();	
			}
			else
			{
				Toast.makeText(this, "No Internet Connection found!", Toast.LENGTH_LONG).show();
				setProgressBarVisibility(false);
			}
		}
		else
		{
			//loadFromDataBase();
			season=db.getSeasonByUrlID(CURRENT_SEASON_URL_ID);
			DatabaseTask task = new DatabaseTask();
			task.execute();
		}
		
	}
	
	private void loadFromDataBase()
	{
		ApplicationDatabase db = new ApplicationDatabase(this);
		allTeams.clear();
		allTeams.addAll(db.getAllTeams());
		//teamsAdapter.notifyDataSetChanged();
	}
	/**
	 * 
	 * @return data inserted or not yet
	 */
	private boolean isInDatabase()
	{
		ApplicationDatabase db = new ApplicationDatabase(this);
		return db.isSeasonExist(CURRENT_SEASON_URL_ID);
	}
	/**
	 * Issues a new Http request to fetch the data.The method uses Google Volley 
	 * Library for creating http request see google volley docs for how to use:<br/>
	 * https://android.googlesource.com/platform/frameworks/volley/
	 */
	private void startRequest()
	{
		// Instantiate the RequestQueue.
				RequestQueue queue = Volley.newRequestQueue(this);
				String url ="http://api.football-data.org/alpha/soccerseasons/351/teams";				
				 
				// Request a string response from the provided URL.
				JsonObjectRequest jsObjRequest = new JsonObjectRequest
				        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
				    @Override
				    public void onResponse(JSONObject response) {
				       
				    	ParsingTask task=new ParsingTask();
				    	task.execute(response);
				    }				    
				    
				}, new Response.ErrorListener() {
				    @Override
				    public void onErrorResponse(VolleyError error) {
				       // mTextView.setText("That didn't work!");
				       // fragment.setResponseText("That didn't work!");
				        setProgressBarVisibility(false);
				        if(error !=null)
				        System.out.println(error.getMessage());
				        ApplicationDatabase db = new ApplicationDatabase(MainActivity.this);
				        db.deleteAllData();
				        
				    }
				})
				{     
			        @Override
			        public Map<String, String> getHeaders() throws AuthFailureError { 
			                Map<String, String>  params = new HashMap<String, String>();  
			                params.put("X-Auth-Token", "328e89987a3043bea2119ed71ec75fc0");  
			              //  params.put("Accept-Language", "fr");

			                return params;  
			        }
				};
				// Add the request to the RequestQueue.
				
				queue.add(jsObjRequest);
	}
	
	class DatabaseTask extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			loadFromDataBase();
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			teamsAdapter.notifyDataSetChanged();
			setProgressBarVisibility(false);
		}
		
	}
	class ParsingTask extends AsyncTask<JSONObject, Void, Void>
	{
       @Override
	    protected void onPreExecute() {
	    	// TODO Auto-generated method stub
	    	super.onPreExecute();
	    	setProgressBarVisibility(true);
	    }
		@Override
		protected Void doInBackground(JSONObject... params) {
			// TODO Auto-generated method stub
			decodeResponseAsync(params[0]);
			return null;
		}
		@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				teamsAdapter.notifyDataSetChanged();
				setProgressBarVisibility(false);
			}
		
	}
	
	private void decodeResponseAsync(JSONObject response)
	{
		String imagesDir = getExternalFilesDir(null)+File.separator+"Images";
		File imagesd=new File(imagesDir);
		imagesd.mkdir();
		ApplicationDatabase db = new ApplicationDatabase(this);
		try {
			 JSONArray arry=  response.getJSONArray("teams");
			
			 for(int i=0;i<arry.length();i++)
			 {
				 JSONObject team =  arry.getJSONObject(i);
				/* builder.append("Team: ");
				 builder.append(team.getString("name"));
				 builder.append("\n");*/
				 JSONObject linksObject = team.getJSONObject("_links");
				 JSONObject fixturesLinkObj = linksObject.getJSONObject("fixtures");
				 String teamFixturesLink = fixturesLinkObj.getString("href");
				 
				 String teamName = team.getString("name");
				 String teamPhoto = team.getString("crestUrl");
				 StringBuilder builder= new StringBuilder(teamPhoto);
				 builder.insert(4, 's');teamPhoto=builder.toString();
				 
				 System.out.println("Downloading Image at:"+teamPhoto);
				 File imageFile = new File( imagesDir,teamName+".svg");
				 System.out.println("Image save at:"+imageFile.getAbsolutePath());
				 String out = downloadTeamImage(teamPhoto,imageFile.getAbsolutePath());
				 if(out!=null)
				 {
					 teamPhoto= imageFile.getAbsolutePath();
				 }
				 else
				 {
					 System.out.println("Error Downloading team Image");
					 teamPhoto = null;
				 }
				 
				 Team teamObj = new Team();
				 teamObj.setTeamName(teamName);
				 teamObj.setTeamPhoto(teamPhoto);
				 teamObj.setFixturesUrl(teamFixturesLink);
				 db.insertTeam(teamObj);
				 allTeams.add(teamObj);						
			 }
			 //teamsAdapter.notifyDataSetChanged();
			// fragment.setResponseText(builder.toString());
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * returns the last occurrence of c
	 * @param st 
	 * @param c
	 * @return
	 */
	static int getLastIndexOf(String st,char c)
	{
		int ret =0;
		for(int i=0;i<st.length();i++)
			if(st.charAt(i)==c)
				ret=i;
		return ret;
	}
	/**
	 * Download image from the specified url
	 * 
	 */
	String downloadTeamImage(String urlst,String name)
	{
		InputStream input = null;
        OutputStream output = null;
       // urlst = new String(urlst.getBytes(), Charset.forName("ISO-8859-15"));
        HttpURLConnection connection = null;
        try {
        	int lastSlash = getLastIndexOf(urlst,'/');
        	String st = urlst.substring(lastSlash+1);//URLEncoder.encode(urlst);
        	//encoding no-ascii file names
        	st = URLEncoder.encode(st);
        	urlst = urlst.substring(0, lastSlash+1)+st;
            URL url =  new URL(urlst);
          
            connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestProperty("Accept-Charset", "UTF-8");
           // connection.setRequestProperty("Content-Type", "image/svg+xml; charset=ISO-8859-15");
            connection.connect();
            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());
                return null;
            }
            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = 0;//connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(name);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                /*if (isCancelled()) {
                    input.close();
                    return null;
                }*/
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    ;//publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
            //return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            	ignored.printStackTrace();
            }

            if (connection != null)
                connection.disconnect();
        }
        return name;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class TeamListItem extends LinearLayout
	{
		private TextView teamNameTxt;
		private ImageView teamPhotoImage;
		SVGImageView svgImageView;
		
		public TeamListItem(Context context) {
			super(context);
			LayoutInflater.from(context).inflate(R.layout.teams_list_item, this);
			teamNameTxt= (TextView) findViewById(R.id.team_name);
			RelativeLayout photoContainer = (RelativeLayout) findViewById(R.id.photo_container);
			teamPhotoImage= (ImageView) findViewById(R.id.team_photo);
			teamPhotoImage.setLayerType(LAYER_TYPE_SOFTWARE, null);
			
			svgImageView = new SVGImageView(context);
			photoContainer.addView(svgImageView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
					RelativeLayout.LayoutParams.MATCH_PARENT));
			
		}
		public void setTeamName(String name)
		{
			teamNameTxt.setText(name);
		}
		public void setTeamPhoto(String name)
		{
			//teamNameTxt.setText(name);
			if(name != null)
			{
				svgImageView.setVisibility(VISIBLE);
				teamPhotoImage.setVisibility(GONE);
				try {
					InputStream is = new FileInputStream(name);
					SVG svg =SVG.getFromInputStream(is);
					svgImageView.setSVG(svg);
					//teamPhotoImage.setImageDrawable( svg.createPictureDrawable());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				teamPhotoImage.setImageResource(R.drawable.cam_placeholder);
				svgImageView.setVisibility(GONE);
				teamPhotoImage.setVisibility(VISIBLE);
			}
		}
		public void setAssociatedTag(String tag)
		{
			svgImageView.setTag(tag);
		}
		public String getAssociatedTag()
		{
			return (String)svgImageView.getTag();
		}
		public void setTeamPhoto(SVG svg)
		{
			svgImageView.setSVG(svg);
		}
		public void hidePlaceHolder()
		{
			teamPhotoImage.setVisibility(GONE);
		}
		
	}
	
	public class TeamsAdapter extends ArrayAdapter<Team>
	{
		
		public TeamsAdapter(Context context, List<Team> objects) {
			super(context, 0, objects);
			// TODO Auto-generated constructor stub
		}
		
		public TeamsAdapter(Context context, int resource, List<Team> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TeamListItem view = (TeamListItem) convertView;
			if(view==null)
			{
				view = new TeamListItem(getContext());				
			}
			Team team = getItem(position);
			view.setTeamName(team.getTeamName());
			view.setAssociatedTag(team.getTeamName());
			String teamPhoto = team.getTeamPhoto();
			if(teamPhoto==null)
				view.setTeamPhoto(teamPhoto);
			else
			{
				view.hidePlaceHolder();
				loadSVG(teamPhoto,view.svgImageView);
			}
			
			return view;//super.getView(position, convertView, parent);
		}
		
		
		
		
		
		
		public void loadSVG(String path, SVGImageView imageView) {
			BitmapWorkerTask  bitmapTask = new BitmapWorkerTask(imageView);
			bitmapTask.setTag((String)imageView.getTag());
			bitmapTask.execute(path);
		}
		
		
		class AsyncDrawable extends SVG {
		    private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

		    public AsyncDrawable(BitmapWorkerTask bitmapWorkerTask) {
		        super();
		        bitmapWorkerTaskReference =
		            new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
		    }

		    public BitmapWorkerTask getBitmapWorkerTask() {
		        return bitmapWorkerTaskReference.get();
		    }
		}
		
		/**
		 * 
		 * @author Mahmoud Galal <br/>
		 *  AsyncTask for decoding SVG Images.Android does not support svg format natively
		 *  and the restful api provides the team icon image in SVG format so we make use of
		 *  external Library Called  "Androidsvg" for decoding svg images.for examples on how to use
		 *  androidsvg see:<br/> https://code.google.com/p/androidsvg/
		 */
		class BitmapWorkerTask extends AsyncTask<String, Void, SVG> {
		    private final WeakReference<SVGImageView> imageViewReference;
		    private String data = null;
		    private String tag = null;

		    public BitmapWorkerTask(SVGImageView imageView) {
		        // Use a WeakReference to ensure the ImageView can be garbage collected
		        imageViewReference = new WeakReference<SVGImageView>(imageView);
		    }
		    public void setTag(String tagobj)
		    {
		    	tag=tagobj;
		    }

		    // Decode image in background.
		    @Override
		    protected SVG doInBackground(String... params) {
		        data = params[0];
		        SVG svg =null;
		        try {
					InputStream is = new FileInputStream(data);
					svg =SVG.getFromInputStream(is);
					
					//teamPhotoImage.setImageDrawable( svg.createPictureDrawable());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        return svg;//decodeSampledBitmapFromResource(getResources(), data, 100, 100));
		    }

		    // Once complete, see if ImageView is still around and set bitmap.
		    @Override
		    protected void onPostExecute(SVG bitmap) {
		        if (imageViewReference != null && bitmap != null) {
		            final SVGImageView imageView = imageViewReference.get();
		            if (imageView != null) {
		            	if(((String)imageView.getTag()).equalsIgnoreCase(tag))
		            	{
		            		imageView.setVisibility(View.VISIBLE);
		            		imageView.setSVG(bitmap);
		            	}
		            }
		        }
		    }
		}
		
	}
	

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private TextView myText;
		private ListView teamsList;
		private TeamsAdapter adapter;
		public PlaceholderFragment() {
		}
		public PlaceholderFragment(TeamsAdapter adapter) {
			this.adapter = adapter;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			//myText = (TextView) rootView.findViewById(R.id.textView1);
			teamsList = (ListView) rootView.findViewById(R.id.teams_list);
			teamsList.setAdapter(adapter);
			teamsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapter, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(), TeamDetailsActivity.class);
					Team team =  (Team) adapter.getItemAtPosition(position);
					intent.putExtra(TEAM_DETAILS_KEY, team.createBundle());
					intent.putExtra(SEASON_ID_KEY, season.getId());
					startActivity(intent);
				}
			});
			return rootView;
		}
		void setTeamListAdapter(TeamsAdapter adapter)
		{
			//teamsList.setAdapter(adapter);
			this.adapter = adapter;
		}
		void setResponseText(String txt)
		{
			if(myText != null)
			myText.setText(txt);
		}
	}
}
