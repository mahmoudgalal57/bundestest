package com.example.bundstest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGImageView;
import com.example.bundstest.MainActivity.ParsingTask;
import com.example.bundstest.MainActivity.TeamListItem;
import com.example.bundstest.MainActivity.TeamsAdapter.BitmapWorkerTask;
import com.example.bundstest.beans.Team;
import com.example.bundstest.beans.TeamFixture;

import database.ApplicationDatabase;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Mahmoud Galal
 *
 */
public class TeamDetailsActivity extends Activity {
  
	private Team currentTeam;
	
	private ListView fixturesList;
	private ArrayList<TeamFixture> allFixtures;
	private FixturesAdapter fixtureAdapter;
	private long seasonId=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_team_details);
		fixturesList = (ListView) findViewById(R.id.fixtures_list);
		currentTeam = Team.getFromBundle(getIntent().getBundleExtra(MainActivity.TEAM_DETAILS_KEY));
		seasonId = getIntent().getLongExtra(MainActivity.SEASON_ID_KEY, 0);
		setTitle(currentTeam.getTeamName());
		allFixtures =  new ArrayList<TeamFixture>();
		fixtureAdapter = new FixturesAdapter(this, allFixtures);
		fixturesList.setAdapter(fixtureAdapter);
		setProgressBarIndeterminate(true);
		
		//startFixturesRequest();
		LoadingTask loadTask = new LoadingTask();
		loadTask.execute();
	}
	
	private void startFixturesRequest()
	{
		setProgressBarVisibility(true);
		RequestQueue queue = Volley.newRequestQueue(this);
		String url = currentTeam.getFixturesUrl();
		
		JsonObjectRequest jsObjRequest = new JsonObjectRequest
		        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
		    @Override
		    public void onResponse(JSONObject response) {
		        
		    	ParsingTask task=new ParsingTask();
		    	task.execute(response);
		    	
		    	
		    }
		}, new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		       
		    	System.out.println(error.getMessage());
		    	setProgressBarVisibility(false);
		    }
		})
		{     
	        @Override
	        public Map<String, String> getHeaders() throws AuthFailureError { 
	                Map<String, String>  params = new HashMap<String, String>();  
	                params.put("X-Auth-Token", "328e89987a3043bea2119ed71ec75fc0");  
	              //  params.put("Accept-Language", "fr");

	                return params;  
	        }
		};
		// Add the request to the RequestQueue.
		queue.add(jsObjRequest);
	}
	
	class LoadingTask extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			allFixtures.clear();
			ApplicationDatabase db = new ApplicationDatabase(TeamDetailsActivity.this);
			allFixtures.addAll(db.getTeamFixtures(currentTeam.getId()));
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			fixtureAdapter.notifyDataSetChanged();
			if(allFixtures.isEmpty())
			{
				System.out.println("Database is empty for team Id: "+currentTeam.getId());
				startFixturesRequest();
			}
			else
				setProgressBarVisibility(false);
		}
		
	}
	
	class ParsingTask extends AsyncTask<JSONObject, Void, Void>
	{
       @Override
	    protected void onPreExecute() {
	    	// TODO Auto-generated method stub
	    	super.onPreExecute();
	    	setProgressBarVisibility(true);
	    }
		@Override
		protected Void doInBackground(JSONObject... params) {
			// TODO Auto-generated method stub
			decodeResponseAsync(params[0]);
			return null;
		}
		@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				fixtureAdapter.notifyDataSetChanged();
				setProgressBarVisibility(false);
			}
		
	}
	
	private void decodeResponseAsync(JSONObject response)
	{
		try {
			 JSONArray arry=  response.getJSONArray("fixtures");
			 String imagesDir = getExternalFilesDir(null)+File.separator+"Images"+File.separator;
			 for(int i=0;i<arry.length();i++)
			 {
				 
				 JSONObject fixture =  arry.getJSONObject(i);
				 
				 JSONObject linksObject = fixture.getJSONObject("_links");
				 JSONObject seasonObject = linksObject.getJSONObject("soccerseason");
				 String seasonUrl= seasonObject.getString("href");
				 if(!seasonUrl.equalsIgnoreCase(MainActivity.CURRENT_SEASON_URL))
					 continue;
				
				 JSONObject result = fixture.getJSONObject("result");						
				 int goalsHomeTeam = result.getInt("goalsHomeTeam");
				 int goalsAwayTeam = result.getInt("goalsAwayTeam");
				 
				 String homeTeamName = fixture.getString("homeTeamName");
				 String awayTeamName = fixture.getString("awayTeamName");
				 
				 TeamFixture teamFixObj = new TeamFixture();
				 teamFixObj.setAwayTeamName(awayTeamName);
				 teamFixObj.setHomeTeamName(homeTeamName);
				 teamFixObj.setGoalsHomeTeam(goalsHomeTeam);
				 teamFixObj.setGoalsAwayTeam(goalsAwayTeam);
				 teamFixObj.setAwayTeamPhoto(imagesDir+awayTeamName+".svg");
				 teamFixObj.setHomeTeamPhoto(imagesDir+homeTeamName+".svg");
				 teamFixObj.setTeamId(currentTeam.getId());
				 teamFixObj.setSeasonId(seasonId);
				 
				 ApplicationDatabase db = new ApplicationDatabase(this);
				 db.insertTeamFixture(teamFixObj);
				 
				 allFixtures.add(teamFixObj);
			 }
			 //fixtureAdapter.notifyDataSetChanged();
			// teamsAdapter.notifyDataSetChanged();
			// fragment.setResponseText(builder.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public class FixtureListItem extends LinearLayout
	{
		private TextView resultTxt;
		private ImageView homeTeamPhotoImage,awayTeamPhotoImage;
		SVGImageView homeSvgImageView,awaySvgImageView;
		 private  WeakReference<BitmapWorkerTask> homeImageTaskReference,awayImageTaskReference;
		public static final int HOME_ASS_ID =10,AWAY_ASS_ID=11;
		
		public FixtureListItem(Context context) {
			super(context);
			LayoutInflater.from(context).inflate(R.layout.fixtures_list_item, this);
			resultTxt = (TextView) findViewById(R.id.match_result);
			homeTeamPhotoImage= (ImageView) findViewById(R.id.home_team_photo);
			awayTeamPhotoImage= (ImageView) findViewById(R.id.awat_team_photo);
			
			RelativeLayout homephotoContainer = (RelativeLayout) findViewById(R.id.home_photo_container);
			RelativeLayout awayphotoContainer = (RelativeLayout) findViewById(R.id.away_photo_container);
			
			homeSvgImageView = new SVGImageView(context);
			awaySvgImageView = new SVGImageView(context);
			homephotoContainer.addView(homeSvgImageView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
					RelativeLayout.LayoutParams.MATCH_PARENT));
			awayphotoContainer.addView(awaySvgImageView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
					RelativeLayout.LayoutParams.MATCH_PARENT));
			
			OnClickListener clistener = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String st = (String)v.getTag();
					Toast.makeText(TeamDetailsActivity.this, st, Toast.LENGTH_SHORT).show();
				}
			};
			homeSvgImageView.setOnClickListener(clistener);
			awaySvgImageView.setOnClickListener(clistener);
			homeTeamPhotoImage.setOnClickListener(clistener);
			awayTeamPhotoImage.setOnClickListener(clistener);
			
		}
		public void setResultTxt(String result)
		{
			resultTxt.setText(result);
		}
		public void setHomeImageAssociatedTag(String name)
		{
			homeSvgImageView.setTag(name);
			homeTeamPhotoImage.setTag(name);
		}
		public void setHomeAssociatedTask(BitmapWorkerTask task)
		{
			WeakReference<BitmapWorkerTask> bmwt = 	 (WeakReference<BitmapWorkerTask>) homeSvgImageView.getTag(HOME_ASS_ID);
			if(bmwt != null)
			{
				bmwt.get().cancel(true);
			}
			homeSvgImageView.setTag(HOME_ASS_ID,new WeakReference<BitmapWorkerTask>(task));
		}
		public void setAwayAssociatedTask(BitmapWorkerTask task)
		{
			WeakReference<BitmapWorkerTask> bmwt = 	 (WeakReference<BitmapWorkerTask>) awaySvgImageView.getTag(AWAY_ASS_ID);
			if(bmwt != null)
			{
				bmwt.get().cancel(true);
			}
			awaySvgImageView.setTag(AWAY_ASS_ID,new WeakReference<BitmapWorkerTask>(task));
		}
		public void setAwayImageAssociatedTag(String name)
		{
			awaySvgImageView.setTag(name);
			awayTeamPhotoImage.setTag(name);
		}
		public void setHomeTeamImage(String name)
		{
			if(name != null)
			{
				homeSvgImageView.setVisibility(VISIBLE);
				homeTeamPhotoImage.setVisibility(GONE);
				try {
					InputStream is = new FileInputStream(name);
					SVG svg =SVG.getFromInputStream(is);
					homeSvgImageView.setSVG(svg);
					//teamPhotoImage.setImageDrawable( svg.createPictureDrawable());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				homeTeamPhotoImage.setImageResource(R.drawable.cam_placeholder);
				homeSvgImageView.setVisibility(GONE);
				homeTeamPhotoImage.setVisibility(VISIBLE);
			}
		}
		public void setAwayTeamImage(String name)
		{
			if(name != null)
			{
				awaySvgImageView.setVisibility(VISIBLE);
				awayTeamPhotoImage.setVisibility(GONE);
				try {
					InputStream is = new FileInputStream(name);
					SVG svg =SVG.getFromInputStream(is);
					awaySvgImageView.setSVG(svg);
					//teamPhotoImage.setImageDrawable( svg.createPictureDrawable());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				awayTeamPhotoImage.setImageResource(R.drawable.cam_placeholder);
				awaySvgImageView.setVisibility(GONE);
				awayTeamPhotoImage.setVisibility(VISIBLE);
			}
		}
		
	}
	
	public class FixturesAdapter extends ArrayAdapter<TeamFixture>
	{
		public static final int HOME_ASS_ID =10,AWAY_ASS_ID=11;
		public FixturesAdapter(Context context, List<TeamFixture> objects) {
			super(context, 0, objects);
			// TODO Auto-generated constructor stub
		}
		
		public FixturesAdapter(Context context, int resource, List<TeamFixture> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			FixtureListItem view = (FixtureListItem) convertView;
			if(view==null)
			{
				view = new FixtureListItem(getContext());				
			}
			TeamFixture teamFix = getItem(position);
			//view.setTeamName(teamFix.getTeamName());
			view.setResultTxt(teamFix.getGoalsHomeTeam()+" - "+teamFix.getGoalsAwayTeam());
			
			view.setHomeImageAssociatedTag(teamFix.getHomeTeamName());
			view.setAwayImageAssociatedTag(teamFix.getAwayTeamName());
			
			String imagesDir = getExternalFilesDir(null)+File.separator+"Images"+File.separator;
			
			String homeTeamImg = imagesDir+teamFix.getHomeTeamName()+".svg";
			String awayTeamImg = imagesDir+teamFix.getAwayTeamName()+".svg";
			
			File imgFile = new File(homeTeamImg);
			if(imgFile.exists())
			{
				//view.setHomeTeamImage(homeTeamImg);
				loadSVG(homeTeamImg,view.homeSvgImageView,HOME_ASS_ID);
			}
			else
				view.setHomeTeamImage(null);
			
			imgFile = new File(awayTeamImg);
			if(imgFile.exists())
			{
				//view.setAwayTeamImage(awayTeamImg);
				loadSVG(awayTeamImg,view.awaySvgImageView,AWAY_ASS_ID);
			}
			else
				view.setAwayTeamImage(null);
			
			
			
			return view;//super.getView(position, convertView, parent);
		}
		
		public void loadSVG(String path, SVGImageView imageView) {
			BitmapWorkerTask  bitmapTask = new BitmapWorkerTask(imageView);
			bitmapTask.setTag((String)imageView.getTag());
			bitmapTask.execute(path);
		}
		
		public void loadSVG(String path, SVGImageView imageView,int assId) {
			BitmapWorkerTask  bitmapTask = new BitmapWorkerTask(imageView);
			bitmapTask.setTag((String)imageView.getTag());
			
			int id = assId == HOME_ASS_ID?R.id.home_association: R.id.away_association;
			WeakReference<BitmapWorkerTask> bmwt = 	 (WeakReference<BitmapWorkerTask>) imageView.getTag(id);
			if(bmwt != null /*&& bmwt instanceof WeakReference<?> */)
			{
				BitmapWorkerTask oldTask = bmwt.get();
						if(oldTask != null)
							oldTask.cancel(true);
			}
			imageView.setTag(id,new WeakReference<BitmapWorkerTask>(bitmapTask));
			bitmapTask.execute(path);
		}
		
		
		class BitmapWorkerTask extends AsyncTask<String, Void, SVG> {
		    private final WeakReference<SVGImageView> imageViewReference;
		    private String data = null;
		    private String tag = null;

		    public BitmapWorkerTask(SVGImageView imageView) {
		        // Use a WeakReference to ensure the ImageView can be garbage collected
		        imageViewReference = new WeakReference<SVGImageView>(imageView);
		    }
		    public void setTag(String tagobj)
		    {
		    	tag=tagobj;
		    }
		    @Override
		    protected void onPreExecute() {
		    	// TODO Auto-generated method stub
		    	super.onPreExecute();
		    	//setProgressBarIndeterminate(true);
		    	setProgressBarIndeterminateVisibility(true);
		    	 final SVGImageView imageView = imageViewReference.get();
		    	 if(imageView != null)
		    		 imageView.setVisibility(View.GONE);
		    }

		    // Decode image in background.
		    @Override
		    protected SVG doInBackground(String... params) {
		        data = params[0];
		        SVG svg =null;
		        if(!isCancelled())
		        try {
					InputStream is = new FileInputStream(data);
					svg =SVG.getFromInputStream(is);
					
					//teamPhotoImage.setImageDrawable( svg.createPictureDrawable());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        return svg;//decodeSampledBitmapFromResource(getResources(), data, 100, 100));
		    }

		    // Once complete, see if ImageView is still around and set bitmap.
		    @Override
		    protected void onPostExecute(SVG bitmap) {
		        if (imageViewReference != null && bitmap != null) {
		            final SVGImageView imageView = imageViewReference.get();
		            if(!isCancelled())
		            if (imageView != null) {
		            	if(((String)imageView.getTag()).equalsIgnoreCase(tag))
		            	{
		            		File imgFile = new File(data);
		        			if(imgFile.exists())
		        			{
			            		imageView.setVisibility(View.VISIBLE);
			            		imageView.setSVG(bitmap);
			            		setProgressBarIndeterminateVisibility(false);
		        			}
		        			else
		        			{
		        				imageView.setVisibility(View.GONE);
		        				setProgressBarIndeterminateVisibility(false);
		        			}
		            	}
		            }
		        }
		    }
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.team_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
