package com.example.bundstest.beans;

/**
 * 
 * @author Mahmoud Galal
 *
 */
public class TeamFixture {

	private String homeTeamName,awayTeamName;
	private String homeTeamPhoto,awayTeamPhoto;
	private int goalsHomeTeam,goalsAwayTeam;
	private long id;
	private long seasonId;
	private long teamId;
	public static final String TABLE_NAME="TEAM_FIXTURES";
	
	public static final String ID_KEY="FIXTURE_ID";
	public static final String HOME_TEAM_NAME_KEY="HOME_TEAM_NAME";
	public static final String AWAY_TEAM_NAME_KEY="AWAY_TEAM_NAME";
	public static final String HOME_TEAM_PHOTO_KEY="HOME_TEAM_PHOTO";
	public static final String AWAY_TEAM_PHOTO_KEY="AWAY_TEAM_PHOTO";
	public static final String AWAY_GOALS_KEY="AWAY_GOALS";
	public static final String HOME_GOALS_KEY="HOME_GOALS";
	public static final String SEASON_ID_KEY="SEASON_ID";
	public static final String TEAM_ID_KEY="TEAM_ID";
	
	
	public TeamFixture() {
		// TODO Auto-generated constructor stub
	}
	public String getHomeTeamName() {
		return homeTeamName;
	}
	public void setHomeTeamName(String homeTeam) {
		this.homeTeamName = homeTeam;
	}
	public String getAwayTeamName() {
		return awayTeamName;
	}
	public void setAwayTeamName(String awayTeam) {
		this.awayTeamName = awayTeam;
	}
	public int getGoalsHomeTeam() {
		return goalsHomeTeam;
	}
	public void setGoalsHomeTeam(int goalsHomeTeam) {
		this.goalsHomeTeam = goalsHomeTeam;
	}
	public int getGoalsAwayTeam() {
		return goalsAwayTeam;
	}
	public void setGoalsAwayTeam(int goalsAwayTeam) {
		this.goalsAwayTeam = goalsAwayTeam;
	}
	public String getHomeTeamPhoto() {
		return homeTeamPhoto;
	}
	public void setHomeTeamPhoto(String homeTeamPhoto) {
		this.homeTeamPhoto = homeTeamPhoto;
	}
	public String getAwayTeamPhoto() {
		return awayTeamPhoto;
	}
	public void setAwayTeamPhoto(String awayTeamPhoto) {
		this.awayTeamPhoto = awayTeamPhoto;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getSeasonId() {
		return seasonId;
	}
	public void setSeasonId(long seasonId) {
		this.seasonId = seasonId;
	}
	public long getTeamId() {
		return teamId;
	}
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}

}
