package com.example.bundstest.beans;

import java.util.ArrayList;

import android.os.Bundle;
import android.text.GetChars;

/**
 * 
 * @author Mahmoud Galal
 *
 */
public class Team {
	private String teamName;
	
	private String teamPhoto;
	private ArrayList<TeamFixture> fixtures;
	private String fixturesUrl;
	private long id;
	
	public static  final String TABLE_NAME="TEAMS";
	
	public static final String ID_KEY="TEAM_ID";
	public static final String NAME_KEY="TEAM_NAME";
	public static final String PHOTO_KEY="TEAM_PHOTO";
	public static final String FIXTURES_URL_KEY="TEAM_FIXTURES_URL";
	
	public static final String TEAM_NAME= Team.class.getName()+".TEAM_NAME";
	public static final String TEAM_PHOTO= Team.class.getName()+".TEAM_PHOTO";
	public static final String FIXTUREURL= Team.class.getName()+".FIXTUREURL";
	public static final String TEAM_ID= Team.class.getName()+".TEAM_ID";
	
	public Team() {
		
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamPhoto() {
		return teamPhoto;
	}

	public void setTeamPhoto(String teamPhoto) {
		this.teamPhoto = teamPhoto;
	}

	public ArrayList<TeamFixture> getFixtures() {
		return fixtures;
	}

	public void setFixtures(ArrayList<TeamFixture> fixtures) {
		this.fixtures = fixtures;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFixturesUrl() {
		return fixturesUrl;
	}

	public void setFixturesUrl(String fixturesUrl) {
		this.fixturesUrl = fixturesUrl;
	}
	
	public Bundle createBundle()
	{
		Bundle bundle = new Bundle();
		bundle.putLong(TEAM_ID, id);
		bundle.putString(TEAM_NAME, teamName);
		bundle.putString(TEAM_PHOTO, teamPhoto);
		bundle.putString(FIXTUREURL, fixturesUrl);
		return bundle;
	}
	public static Team getFromBundle(Bundle bundle)
	{
		Team team = new Team();
		team.setId(bundle.getLong(TEAM_ID));
		team.setTeamName(bundle.getString(TEAM_NAME));
		team.setTeamPhoto(bundle.getString(TEAM_PHOTO));
		team.setFixturesUrl(bundle.getString(FIXTUREURL));
		return team;
	}

}
