package com.example.bundstest.beans;

/**
 * 
 * @author Mahmoud Galal
 *
 */
public class Season {

	private long id;
	private int urlID;
	public static final String TABLE_NAME="SEASONS";
	public static final String ID_KEY="SEASON_ID";
	public static final String URL_ID="SEASON_URL_ID";
	public Season() {
		// TODO Auto-generated constructor stub
		
	}
	public int getUrlID() {
		return urlID;
	}
	public void setUrlID(int urlID) {
		this.urlID = urlID;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

}
