**A simple example showing how to fetch data from external restful API asynchronously 
and distributing it over two screens and caching data in a database. For convenience you will find a pre-built APK in the *bin* folder**  
How to compile :
Just import the project in eclipse!
Notes:
-the app uses "[androidsvg](https://code.google.com/p/androidsvg/)" library for rendering SVG images. For convenience you will find a pre-built jar in the lib folder.
-the app uses "[Google Volley](https://android.googlesource.com/platform/frameworks/volley/)" for sending Http Requests. For your convenience you will find a pre-built jar in the lib folder.

![device-2015-06-26-211013.png](https://bitbucket.org/repo/9zR4EK/images/70752937-device-2015-06-26-211013.png)

![device-2015-06-26-211101.png](https://bitbucket.org/repo/9zR4EK/images/2569715544-device-2015-06-26-211101.png)